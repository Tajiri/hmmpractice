import numpy as np

#t=0, 1, 2, 3, 4, ....., n-1.
A=np.array([[0.1,0.7,0.2],[0.2,0.1,0.7],[0.7,0.2,0.1]])
B=np.array([[0.9,0.1],[0.6,0.4],[0.1,0.9]])
c=3
m=2

n=3
rho=np.repeat(1/3,3)
x=(0,1,0)

#initial
alpha=[rho[i]*B[i,x[0]] for i in range(c)]

# recursive
for t in range(1, n):
    alpha = np.array([sum([alpha[i] * A[i, j] for i in range(c)]) 
                      * B[j, x[t]] for j in range(c)])
    print("t=", t, " alpha = ", alpha)

#step3
P = sum(alpha)
print(P)